﻿
$(function () {
    $(window).on('load resize', function () {
        $('.fill-screen').css('height', window.innerHeight);
        var textBannerHeight = $('.text-banner').height();
        var currentTimeHeaderHeight = $('.currenttime-header').height();
        var currentTimeButtonHeight = $('.currenttime-button').height();
        var paginatorHeight = $('.mat-paginator').height();
        $('.currenttime-container').css('height', window.innerHeight - (textBannerHeight + currentTimeHeaderHeight + currentTimeButtonHeight + paginatorHeight));
    });
    
    // add Bootstrap's scrollspy
    $('body').scrollspy({
        target: '.navbar',
        offset: 160
    });

    // smooth scrolling
    $('nav a, .down-button a').bind('click', function () {
        $('html, body').stop().animate({
            scrollTop: $($(this).attr('href')).offset().top - 60
        }, 1500, 'easeInOutExpo');
        event.preventDefault ? event.preventDefault() : event.returnValue = false;
    });

    $('.mat-table, .ng-tns-c2-1').on('mousewheel DOMMouseScroll', function (e) {
        var scrollTo = null;

        if (e.type === 'mousewheel') {
            scrollTo = (e.originalEvent.wheelDelta * -1);
        }
        else if (e.type === 'DOMMouseScroll') {
            scrollTo = 40 * e.originalEvent.detail;
        }

        if (scrollTo) {
            event.preventDefault ? event.preventDefault() : event.returnValue = false;
            $(this).scrollTop(scrollTo + $(this).scrollTop());
        }
    });

    // parallax scrolling with stellar.js
    $(window).stellar();
});