import * as tslib_1 from "tslib";
import { Component, ViewChild } from '@angular/core';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { TimeZone } from './time-zone';
import { CurrentTimesService } from './current-times.service';
import { TimeZonesService } from './time-zones.service';
/**
 * @title Data table with sorting, pagination, and filtering.
 */
var CurrentTimesComponent = /** @class */ (function () {
    function CurrentTimesComponent(_currentTimesService, _timeZonesService) {
        this._currentTimesService = _currentTimesService;
        this._timeZonesService = _timeZonesService;
        this.pageNumber = 1;
        this.pageSize = 10;
        this.selectedTimeZone = new TimeZone();
        this.orderBy = '';
        this.errorMessage = '';
        this.currentTimes = [];
        this.timeZones = [];
        this.displayedColumns = ['currentTimeId', 'serverTime', 'clientIp', 'utcTime', 'timeZoneId'];
        this.selectedTimeZone.id = '';
        this.getCurrentTimes();
        this.getTimeZones();
    }
    CurrentTimesComponent.prototype.getCurrentTimes = function () {
        var _this = this;
        this._currentTimesService.getCurrentTimes(this.pageNumber, this.pageSize, this.selectedTimeZone.id, this.orderBy).subscribe(function (currenTimes) {
            _this.currentTimes = currenTimes;
            _this.dataSource = new MatTableDataSource(_this.currentTimes);
            _this.dataSource.paginator = _this.paginator;
            _this.dataSource.sort = _this.sort;
        }, function (error) { return _this.errorMessage = error; });
    };
    CurrentTimesComponent.prototype.getTimeZones = function () {
        var _this = this;
        this._timeZonesService.getTimeZones().subscribe(function (timeZones) {
            _this.timeZones = timeZones;
        }, function (error) { return _this.errorMessage = error; });
    };
    CurrentTimesComponent.prototype.pageChange = function () {
        this.pageNumber = this.dataSource.paginator.pageIndex + 1;
        this.pageSize = this.dataSource.paginator.pageSize;
        this.getCurrentTimes();
    };
    CurrentTimesComponent.prototype.sortChange = function () {
        this.orderBy = this.dataSource.sort.active + " " + this.dataSource.sort.direction;
        this.getCurrentTimes();
    };
    CurrentTimesComponent.prototype.submitClick = function () {
        this.getCurrentTimes();
    };
    tslib_1.__decorate([
        ViewChild(MatPaginator),
        tslib_1.__metadata("design:type", MatPaginator)
    ], CurrentTimesComponent.prototype, "paginator", void 0);
    tslib_1.__decorate([
        ViewChild(MatSort),
        tslib_1.__metadata("design:type", MatSort)
    ], CurrentTimesComponent.prototype, "sort", void 0);
    CurrentTimesComponent = tslib_1.__decorate([
        Component({
            selector: 'current-times',
            styleUrls: ['current-times.component.css'],
            templateUrl: 'current-times.component.html',
        }),
        tslib_1.__metadata("design:paramtypes", [CurrentTimesService, TimeZonesService])
    ], CurrentTimesComponent);
    return CurrentTimesComponent;
}());
export { CurrentTimesComponent };
//# sourceMappingURL=current-times.component.js.map