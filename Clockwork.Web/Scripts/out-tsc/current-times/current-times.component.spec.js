import { TestBed, async } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { CurrentTimesComponent } from './current-times.component';
describe('CurrentTimesComponent', function () {
    beforeEach(async(function () {
        TestBed.configureTestingModule({
            imports: [
                RouterTestingModule
            ],
            declarations: [
                CurrentTimesComponent
            ],
        }).compileComponents();
    }));
    it('should create the app', function () {
        var fixture = TestBed.createComponent(CurrentTimesComponent);
        var app = fixture.debugElement.componentInstance;
        expect(app).toBeTruthy();
    });
    it("should have as title 'clockwork-app'", function () {
        var fixture = TestBed.createComponent(CurrentTimesComponent);
        var app = fixture.debugElement.componentInstance;
        expect(app.title).toEqual('clockwork-app');
    });
    it('should render title in a h1 tag', function () {
        var fixture = TestBed.createComponent(CurrentTimesComponent);
        fixture.detectChanges();
        var compiled = fixture.debugElement.nativeElement;
        expect(compiled.querySelector('h1').textContent).toContain('Welcome to clockwork-app!');
    });
});
//# sourceMappingURL=current-times.component.spec.js.map