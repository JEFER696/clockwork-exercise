import * as tslib_1 from "tslib";
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { CurrentTimesComponent } from './current-times.component';
import { HttpClientModule } from '@angular/common/http';
var CurrentTimesModule = /** @class */ (function () {
    function CurrentTimesModule() {
    }
    CurrentTimesModule = tslib_1.__decorate([
        NgModule({
            declarations: [
                CurrentTimesComponent
            ],
            imports: [
                BrowserModule,
                HttpClientModule
            ],
            providers: [],
            bootstrap: [CurrentTimesComponent]
        })
    ], CurrentTimesModule);
    return CurrentTimesModule;
}());
export { CurrentTimesModule };
//# sourceMappingURL=current-times.module.js.map