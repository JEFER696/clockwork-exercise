import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
var TimeZonesService = /** @class */ (function () {
    function TimeZonesService(http) {
        this.http = http;
        this.timeZonesUrl = 'http://localhost:7527/api/timezones';
    }
    TimeZonesService.prototype.getTimeZones = function () {
        var headers = new HttpHeaders({ 'Content-Type': 'application/json' });
        var url = "" + this.timeZonesUrl;
        return this.http.get(url, { headers: headers })
            .pipe(catchError(this.handleError));
    };
    TimeZonesService.prototype.handleError = function (err) {
        // in a real world app, we may send the server to some remote logging infrastructure
        // instead of just logging it to the console
        var errorMessage;
        if (err.error instanceof ErrorEvent) {
            // A client-side or network error occurred. Handle it accordingly.
            errorMessage = "An error occurred: " + err.error.message;
        }
        else {
            // The backend returned an unsuccessful response code.
            // The response body may contain clues as to what went wrong,
            errorMessage = "Backend returned code " + err.status + ": " + err.body.error;
        }
        console.error(err);
        return throwError(errorMessage);
    };
    TimeZonesService = tslib_1.__decorate([
        Injectable({
            providedIn: 'root'
        }),
        tslib_1.__metadata("design:paramtypes", [HttpClient])
    ], TimeZonesService);
    return TimeZonesService;
}());
export { TimeZonesService };
//# sourceMappingURL=time-zones.service.js.map