import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';

import { Observable, of, throwError } from 'rxjs';
import { catchError, tap, map, isEmpty } from 'rxjs/operators';

import { CurrentTimeCollection } from './current-time-collection';
import { isNull } from 'util';
import { Response, headersToString } from 'selenium-webdriver/http';

import { environment } from '../environments/environment';

import { CurrentTime } from './current-time';

@Injectable({
    providedIn: 'root'
})
export class CurrentTimesService {
    private currentTimesUrl = environment.urlAddress + 'api/currenttimes';

    constructor(private http: HttpClient) { }

    getCurrentTimes(pageNumber: number, pageSize: number, timeZoneId: string, orderBy: string): Observable<HttpResponse<CurrentTimeCollection>> {
        const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
        const url = `${this.currentTimesUrl}?pagesize=${pageSize}&pagenumber=${pageNumber}&timezoneid=${timeZoneId}&orderby=${orderBy}`;
        return this.http.get<CurrentTimeCollection>(url, { headers: headers, observe: 'response' })
            .pipe(
                catchError(this.handleError)
            );
    }

    private handleError(err) {
        // in a real world app, we may send the server to some remote logging infrastructure
        // instead of just logging it to the console
        let errorMessage: string;
        if (err.error instanceof ErrorEvent) {
            // A client-side or network error occurred. Handle it accordingly.
            errorMessage = `An error occurred: ${err.error.message}`;
        } else {
            // The backend returned an unsuccessful response code.
            // The response body may contain clues as to what went wrong,
            errorMessage = `Backend returned code ${err.status}: ${err.body.error}`;
        }
        console.error(err);
        return throwError(errorMessage);
    }

}
