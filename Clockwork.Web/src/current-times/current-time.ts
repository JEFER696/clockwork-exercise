/* Defines the CurrentTime entity */
export interface ICurrentTime {
    currentTimeId: string;
    serverTime: string;
    clientIp: string;
    utcTime: string;
    timeZoneId: string;
  }

export class CurrentTime implements ICurrentTime {
    currentTimeId: string;
    serverTime: string;
    clientIp: string;
    utcTime: string;
    timeZoneId: string;
}