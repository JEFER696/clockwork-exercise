import { Component, ViewChild } from '@angular/core';
import { MatPaginator, MatSort, MatTableDataSource, Sort, PageEvent } from '@angular/material';

import { CurrentTime } from './current-time';
import { CurrentTimeCollection } from './current-time-collection';
import { TimeZone } from './time-zone';

import { CurrentTimesService } from './current-times.service';
import { TimeZonesService } from './time-zones.service';
import { concat } from 'rxjs/operators';
import { forEach } from '@angular/router/src/utils/collection';

/**
 * @title Data table with sorting, pagination, and filtering.
 */
@Component({
    selector: 'current-times',
    styleUrls: ['current-times.component.css'],
    templateUrl: 'current-times.component.html',
})
export class CurrentTimesComponent {
    selectedTimeZone: TimeZone = new TimeZone();
    noneOptionTimeZone: TimeZone = new TimeZone();
    timeZones: TimeZone[] = [];
    pageLength: number = 0;
    pageNumber: number = 1;
    pageSize: number = 10;
    orderBy: string = '';
    errorMessage = '';   

    displayedColumns = ['currentTimeId', 'serverTime', 'clientIp', 'utcTime', 'timeZoneId'];

    dataSource: MatTableDataSource<any>;

    constructor(private _currentTimesService: CurrentTimesService, private _timeZonesService: TimeZonesService) {
        this.selectedTimeZone.id = '';
        this.noneOptionTimeZone.id = '';
        this.getCurrentTimes('');
        this.getTimeZones();
    }

    getCurrentTimes(timeZoneId: string, pageNumber: number = this.pageNumber, pageSize: number = this.pageSize, orderBy: string = this.orderBy) {
        this._currentTimesService.getCurrentTimes(pageNumber, pageSize, timeZoneId, orderBy).subscribe(
            resp => {
                let xPagination = resp.headers.get('x-pagination');
                if (this.dataSource === undefined) {
                    this.dataSource = new MatTableDataSource(resp.body.value);
                }
                else {
                    this.dataSource.data = resp.body.value;
                }
                this.pageLength = JSON.parse(xPagination)['totalCount'];
            },
            error => this.errorMessage = <any>error);
    }

    getTimeZones() {
        this._timeZonesService.getTimeZones().subscribe(
            timeZones => {
                this.timeZones = timeZones;
            },
            error => this.errorMessage = <any>error);
    }

    pageChange(pageEvent: PageEvent) {
        this.pageNumber = pageEvent.pageIndex + 1;
        this.pageSize = pageEvent.pageSize;
        this.getCurrentTimes('');
    }

    sortChange(sort: Sort) {
        this.orderBy = sort.active + ' ' + sort.direction;
        this.getCurrentTimes('');
    }

    submitClick() {
        this.getCurrentTimes(this.selectedTimeZone.id);
    }
}