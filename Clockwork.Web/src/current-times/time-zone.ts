/* Defines the TimeZone entity */
export interface ITimeZone {
    id: string;
    displayName: string;
  }

export class TimeZone implements ITimeZone {
  id: string;
  displayName: string;
}