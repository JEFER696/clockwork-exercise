import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { CurrentTimesComponent } from './current-times.component';

import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [
    CurrentTimesComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [CurrentTimesComponent]
})
export class CurrentTimesModule { }
