/* Defines the CurrentTimes entity */
import { CurrentTime } from './current-time';
import { Link } from './link';

export interface ICurrentTimeCollection {
    value: Array<any>;
    links: Link[];
    totalCount: number;
  }

export class CurrentTimeCollection implements ICurrentTimeCollection {
    value: Array<any>;
    links: Link[];
    totalCount: number;
}