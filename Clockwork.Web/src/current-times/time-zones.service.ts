import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable, of, throwError } from 'rxjs';
import { catchError, tap, map, isEmpty } from 'rxjs/operators';

import { TimeZone } from './time-zone';

import { environment } from '../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class TimeZonesService {
    private timeZonesUrl = environment.urlAddress + 'api/timezones';

  constructor(private http: HttpClient) { }

  getTimeZones() : Observable<TimeZone[]> {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' }); 
    const url = `${this.timeZonesUrl}`;
    return this.http.get<TimeZone[]>(url, { headers: headers })
      .pipe(
        catchError(this.handleError)
      );
  }

  private handleError(err) {
    // in a real world app, we may send the server to some remote logging infrastructure
    // instead of just logging it to the console
    let errorMessage: string;
    if (err.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      errorMessage = `An error occurred: ${err.error.message}`;
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      errorMessage = `Backend returned code ${err.status}: ${err.body.error}`;
    }
    console.error(err);
    return throwError(errorMessage);
  }

}
