/* Defines the Link entity */
export interface ILink {
    href: string;
    rel: string;
    method: string;
}

export class Link implements ILink {
    href: string;
    rel: string;
    method: string;
}