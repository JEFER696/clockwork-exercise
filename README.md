README

Good day

This repo contains one solution with two projects.

####Project Clockwork.API####
Clockwork.API is a simple API that returns a time date object and records the IP address, and timedate of any callers to the API in a SQLite database.

####Project Clockwork.Web####
Clockwork.Web is a simple website that uses AngularJS 7 and Asp.Net MVC to call the API at the push of a button and displays the resulting JSON object.

####Required Technologies####
* Node.js.
* A windows or Mac computer.
* Visual Studio Community Edition (latest version).  Note there are missing components on the Mac that you will have to self install (NuGet CLI) to run the API project while running the website.

### How do I get set up? ###

* Install Visual Studio Community 2017 or Visual Studio Code if you do not have it set up.  Note that Visual Studio 2015 will not work with this project.
* Install Node.js. Installer can be downloaded at https://nodejs.org/en/download/.
* Clone this repo.
* Make sure Clockwork.Api/Properties/launchSettings.json "iisSettings":"applicationUrl" is same with Clockwork.Web/src/environments.ts "environment":"urlAddress". To able the AngularJS calling the API.
* Open cmd, go to Clockwork.Web project directory
	- Type npm install then enter
	- Type ng build then enter

### Who do I talk to? ###

* For this repo, questions on direction, scope, or intent can be directed to jefdeguzman78@yahoo.com
