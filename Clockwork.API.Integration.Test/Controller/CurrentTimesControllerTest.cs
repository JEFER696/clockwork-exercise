﻿using Clockwork.API.Integration.Test.Setup;
using Clockwork.API.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Xunit;

namespace Clockwork.API.Integration.Test.Controller
{
    public class CurrentTimesControllerTest : IClassFixture<TestFixture<TestStartupSqlite>>
    {
        public HttpClient Client { get; }

        public CurrentTimesControllerTest(TestFixture<TestStartupSqlite> fixture)
        {
            Client = fixture.httpClient;
        }

        [Fact]
        public async Task When_Request_Is_Valid_GetCurrentTimes_Returns_OK()
        {
            // Arrange
            var request = $"api/currenttimes";

            // Act
            var response = await Client.GetAsync(request);

            // Assert
            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
        }

        [Fact]
        public async Task When_OrderBy_Is_Valid_GetCurrentTimes_Returns_OK()
        {
            // Arrange
            var request = $"api/currenttimes?orderby=timezoneid";

            // Act
            var response = await Client.GetAsync(request);

            // Assert
            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
        }
       
        [Fact]
        public async Task When_OrderBy_IsNot_Valid_GetCurrentTimes_Returns_BadRequest()
        {
            // Arrange
            var request = $"api/currenttimes?orderby=invalidorderby";

            // Act
            var response = await Client.GetAsync(request);

            // Assert
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [Fact]
        public async Task When_TimeZoneId_Is_Valid_GetCurrentTimes_Returns_OK()
        {
            // Arrange
            var request = $"api/currenttimes?timezoneid=utc";

            // Act
            var response = await Client.GetAsync(request);

            // Assert
            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
        }

        [Fact]
        public async Task When_TimeZoneId_IsNot_Valid_GetCurrentTimes_Returns_BadRequest()
        {
            // Arrange
            var request = $"api/currenttimes?timezoneid=invalidtimezoneid";

            // Act
            var response = await Client.GetAsync(request);

            // Assert
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [Fact]
        public async Task When_PageSize_Is_Three_GetCurrentTimes_Returns_Three_Records()
        {
            // Arrange
            var request = $"api/currenttimes?pagesize=3";

            // Act
            await Client.GetAsync(request);
            await Client.GetAsync(request);
            await Client.GetAsync(request);
            await Client.GetAsync(request);
            await Client.GetAsync(request);
            var response = await Client.GetAsync(request);
            var linkedCurrentTimeCollectionResource = JsonConvert.DeserializeObject<LinkedCurrentTimeCollectionResourceDto>(await response.Content.ReadAsStringAsync());
            
            // Assert
            Assert.Equal(3, linkedCurrentTimeCollectionResource.Value.Count());
        }

        [Fact]
        public async Task When_PageNumber_Is_Two_And_PageSize_Is_Two_GetCurrentTimes_Returns_Third_And_Fourth_Records()
        {
            // Arrange
            var request = $"api/currenttimes?pagenumber=2&pagesize=2&orderby=currenttimeid";

            // Act
            await Client.GetAsync(request);
            await Client.GetAsync(request);
            await Client.GetAsync(request);
            await Client.GetAsync(request);
            await Client.GetAsync(request);
            var response = await Client.GetAsync(request);
            var linkedCurrentTimeCollectionResource = JsonConvert.DeserializeObject<LinkedCurrentTimeCollectionResourceDto>(await response.Content.ReadAsStringAsync());

            var linkedCurrentTimeCollectionResourceValueList = linkedCurrentTimeCollectionResource.Value.ToList();
            var thirdRecord = (IDictionary<string, object>)linkedCurrentTimeCollectionResourceValueList[0];
            var fourthRecord = (IDictionary<string, object>)linkedCurrentTimeCollectionResourceValueList[1];
           
            // Assert
            Assert.Equal(2, linkedCurrentTimeCollectionResource.Value.Count());
            Assert.Equal(3, Convert.ToInt32(thirdRecord["currentTimeId"]));
            Assert.Equal(4, Convert.ToInt32(fourthRecord["currentTimeId"]));
        }

        [Theory]
        [InlineData("currenttimeid")]
        [InlineData("currenttimeid,servertime")]
        [InlineData("currenttimeid,servertime,clientip")]
        [InlineData("currenttimeid,servertime,clientip,utctime")]
        [InlineData("currenttimeid,servertime,clientip,utctime,timezoneid")]
        public async Task When_Fields_Param_Is_Provided_GetCurrentTimes_Returns_Object_With_The_Specified_Fields_Only(string fields)
        {
            // Arrange
            var request = $"api/currenttimes?pagenumber=1&pagesize=1&fields={fields}";

            // Act
            var response = await Client.GetAsync(request);
            var linkedCurrentTimeCollectionResource = JsonConvert.DeserializeObject<LinkedCurrentTimeCollectionResourceDto>(await response.Content.ReadAsStringAsync());
            var linkedCurrentTimeCollectionResourceValueList = linkedCurrentTimeCollectionResource.Value.ToList();
            var firstRecord = (IDictionary<string, object>)linkedCurrentTimeCollectionResourceValueList[0];

            // Assert
            switch (fields)
            {
                case "currenttimeid" :
                    Assert.Single(firstRecord.Keys);
                    break;
                case "currenttimeid,servertime":
                    Assert.Equal(2, firstRecord.Keys.Count());
                    Assert.Equal("currenttimeid", firstRecord.Keys.FirstOrDefault(x => x.ToLower() == "currenttimeid").ToLower());
                    Assert.Equal("servertime", firstRecord.Keys.FirstOrDefault(x => x.ToLower() == "servertime").ToLower());
                    break;
                case "currenttimeid,servertime,clientip":
                    Assert.Equal(3, firstRecord.Keys.Count());
                    Assert.Equal("currenttimeid", firstRecord.Keys.FirstOrDefault(x => x.ToLower() == "currenttimeid").ToLower());
                    Assert.Equal("servertime", firstRecord.Keys.FirstOrDefault(x => x.ToLower() == "servertime").ToLower());
                    Assert.Equal("clientip", firstRecord.Keys.FirstOrDefault(x => x.ToLower() == "clientip").ToLower());
                    break;
                case "currenttimeid,servertime,clientip,utctime":
                    Assert.Equal(4, firstRecord.Keys.Count());
                    Assert.Equal("currenttimeid", firstRecord.Keys.FirstOrDefault(x => x.ToLower() == "currenttimeid").ToLower());
                    Assert.Equal("servertime", firstRecord.Keys.FirstOrDefault(x => x.ToLower() == "servertime").ToLower());
                    Assert.Equal("clientip", firstRecord.Keys.FirstOrDefault(x => x.ToLower() == "clientip").ToLower());
                    Assert.Equal("utctime", firstRecord.Keys.FirstOrDefault(x => x.ToLower() == "utctime").ToLower());
                    break;
                case "currenttimeid,servertime,clientip,utctime,timezoneid":
                    Assert.Equal(5, firstRecord.Keys.Count());
                    Assert.Equal("currenttimeid", firstRecord.Keys.FirstOrDefault(x => x.ToLower() == "currenttimeid").ToLower());
                    Assert.Equal("servertime", firstRecord.Keys.FirstOrDefault(x => x.ToLower() == "servertime").ToLower());
                    Assert.Equal("clientip", firstRecord.Keys.FirstOrDefault(x => x.ToLower() == "clientip").ToLower());
                    Assert.Equal("utctime", firstRecord.Keys.FirstOrDefault(x => x.ToLower() == "utctime").ToLower());
                    Assert.Equal("timezoneid", firstRecord.Keys.FirstOrDefault(x => x.ToLower() == "timezoneid").ToLower());
                    break;
                default:
                    break;
            }
        }

        [Theory]
        [InlineData("invalidcurrenttimeid")]
        [InlineData("invalidcurrenttimeid,servertime")]
        [InlineData("currenttimeid,invalidservertime,invalidclientip")]
        [InlineData("currenttimeid,servertime,invalidclientip,utctime")]
        [InlineData("currenttimeid,servertime,clientip,utctime,invalidtimezoneid")]
        public async Task  When_Fields_Param_IsNot_Valid_GetCurrentTimes_Returns_BadRequest(string fields)
        {
            // Arrange
            var request = $"api/currenttimes?pagenumber=1&pagesize=1&fields={fields}";

            // Act
            var response = await Client.GetAsync(request);

            // Assert
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }
    }
}
