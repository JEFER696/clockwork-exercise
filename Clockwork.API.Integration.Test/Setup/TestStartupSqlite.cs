﻿using Clockwork.API.Entities;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Clockwork.API.Integration.Test.Setup
{
    public class TestStartupSqlite : Startup
    {
        public TestStartupSqlite(IConfiguration config) : base(config)
        {
        }

        public override void SetUpDataBase(IServiceCollection services)
        {
            var connectionStringBuilder = new SqliteConnectionStringBuilder
            {
                DataSource = ":memory:"
            };
            var connectionString = connectionStringBuilder.ToString();
            var connection = new SqliteConnection(connectionString);
            services.AddEntityFrameworkSqlite()
                .AddDbContext<ClockworkContext>(options => options.UseSqlite(connection));
        }

        public override void EnsureDatabaseCreated(ClockworkContext dbContext)
        {
            dbContext.Database.OpenConnection();
            dbContext.Database.EnsureCreated();
        }
    }
}
