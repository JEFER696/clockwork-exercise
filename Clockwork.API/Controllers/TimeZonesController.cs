﻿using System;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using Clockwork.API.Models;

namespace Clockwork.API.Controllers
{
    [Route("api/timezones")]
    public class TimeZonesController : Controller
    {
        [HttpGet(Name = "GetTimeZones")]
        public IActionResult GetTimeZones()
        {
            return Ok(TimeZoneInfo.GetSystemTimeZones().Select(x => new TimeZoneDto() { Id = x.Id, DisplayName = x.DisplayName }));
        }
    }
}
