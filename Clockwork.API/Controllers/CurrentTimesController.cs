﻿using System;
using Microsoft.AspNetCore.Mvc;
using Clockwork.API.Services;
using Clockwork.API.Entities;
using Clockwork.API.Helpers;
using AutoMapper;
using Clockwork.API.Models;
using System.Collections.Generic;
using Microsoft.AspNetCore.Http;
using System.Threading.Tasks;

namespace Clockwork.API.Controllers
{
    [Route("api/currenttimes")]
    public class CurrentTimesController : Controller
    {
        private IClockworkRepository _clockworkRepository;
        private IUrlHelper _urlHelper;
        private IPropertyMappingService _propertyMappingService;
        private IHttpContextAccessor _accessor;
        private ITypeHelperService _typeHelperService;

        public CurrentTimesController(IClockworkRepository clockworkRepository,
            IUrlHelper urlHelper,
            IPropertyMappingService propertyMappingService,
            IHttpContextAccessor accessor,
            ITypeHelperService typeHelperService)
        {
            _clockworkRepository = clockworkRepository;
            _urlHelper = urlHelper;
            _propertyMappingService = propertyMappingService;
            _accessor = accessor;
            _typeHelperService = typeHelperService;
        }

        [HttpGet(Name = "GetCurrentTimes")]
        public async Task<IActionResult> GetCurrentTimes(CurrentTimesResourcesParameters currentTimesResourcesParameters)
        {
            if (!_propertyMappingService.ValidMappingExistsFor<CurrentTimeDto, CurrentTime>
               (currentTimesResourcesParameters.OrderBy))
            {
                ModelState.AddModelError(nameof(currentTimesResourcesParameters.OrderBy), "The order by is invalid.");
                return BadRequest();
            }

            if (!currentTimesResourcesParameters.TimeZoneId.IsValid())
            {
                ModelState.AddModelError(nameof(currentTimesResourcesParameters.TimeZoneId), "The time zone id is invalid.");
                return BadRequest(ModelState);
            }

            if (!_typeHelperService.TypeHasProperties<CurrentTimeDto>
               (currentTimesResourcesParameters.Fields))
            {
                ModelState.AddModelError(nameof(currentTimesResourcesParameters.Fields), "Fields are invalid.");
                return BadRequest();
            }

            await this.CreateCurrentTime(currentTimesResourcesParameters);

            var currentTimesFromRepo = await _clockworkRepository.GetCurrentTimes(currentTimesResourcesParameters);

            var currentTimes = Mapper.Map<IEnumerable<CurrentTimeDto>>(currentTimesFromRepo);

            var paginationMetadata = new
            {
                totalCount = currentTimesFromRepo.TotalCount,
                pageSize = currentTimesFromRepo.PageSize,
                currentPage = currentTimesFromRepo.CurrentPage,
                totalPages = currentTimesFromRepo.TotalPages
            };

            Response.Headers.Add("x-pagination",
                Newtonsoft.Json.JsonConvert.SerializeObject(paginationMetadata));

            var links = CreateLinksForCurrentTimes(currentTimesResourcesParameters,
                currentTimesFromRepo.HasNext, currentTimesFromRepo.HasPrevious);

            var shapedCurrentTimes = currentTimes.ShapeData(currentTimesResourcesParameters.Fields);

            var linkedCurrentTimeCollectionResource = new LinkedCurrentTimeCollectionResourceDto()
            {
                Value = shapedCurrentTimes,
                Links = links
            };

            return Ok(linkedCurrentTimeCollectionResource);
        }

        private async Task CreateCurrentTime(CurrentTimesResourcesParameters currentTimesResourcesParameters)
        {
            var utcTime = DateTime.UtcNow;
            var serverTime = DateTime.Now;
            var ip = _accessor.HttpContext?.Connection?.RemoteIpAddress?.ToString();

            var currentTime = new CurrentTimeForCreationDto
            {
                UTCTime = utcTime,
                ClientIp = ip,
                ServerTime = serverTime,
                TimeZoneId = currentTimesResourcesParameters.TimeZoneId
            };

            var currentTimeEntity = Mapper.Map<CurrentTime>(currentTime);

            await _clockworkRepository.AddCurrentTime(currentTimeEntity);

            if (!(await _clockworkRepository.Save()))
            {
                throw new Exception("Creating a Current Time failed on save.");
            }
        }

        private IEnumerable<LinkDto> CreateLinksForCurrentTimes(
            CurrentTimesResourcesParameters currentTimesResourceParameters,
            bool hasNext, bool hasPrevious)
        {
            var links = new List<LinkDto>
            {
                // self 
                new LinkDto(CreateCurrentTimesResourceUri(currentTimesResourceParameters, 
                ResourceUriType.Current),
                "self", "GET")
            };

            if (hasNext)
            {
                links.Add(
                    new LinkDto(CreateCurrentTimesResourceUri(currentTimesResourceParameters, 
                    ResourceUriType.NextPage),
                    "nextPage", "GET"));
            }

            if (hasPrevious)
            {
                links.Add(
                    new LinkDto(CreateCurrentTimesResourceUri(currentTimesResourceParameters,
                    ResourceUriType.PreviousPage),
                    "previousPage", "GET"));
            }

            return links;
        }

        private string CreateCurrentTimesResourceUri(
            CurrentTimesResourcesParameters currentTimesResourcesParameters,
            ResourceUriType type)
        {
            switch (type)
            {
                case ResourceUriType.PreviousPage:
                    return _urlHelper.Link("GetCurrentTimes",
                      new
                      {
                          fields = currentTimesResourcesParameters.Fields,
                          orderBy = currentTimesResourcesParameters.OrderBy,
                          pageNumber = currentTimesResourcesParameters.PageNumber - 1,
                          pageSize = currentTimesResourcesParameters.PageSize
                      });
                case ResourceUriType.NextPage:
                    return _urlHelper.Link("GetCurrentTimes",
                      new
                      {
                          fields = currentTimesResourcesParameters.Fields,
                          orderBy = currentTimesResourcesParameters.OrderBy,
                          pageNumber = currentTimesResourcesParameters.PageNumber + 1,
                          pageSize = currentTimesResourcesParameters.PageSize
                      });
                case ResourceUriType.Current:
                default:
                    return _urlHelper.Link("GetCurrentTimes",
                    new
                    {
                        fields = currentTimesResourcesParameters.Fields,
                        orderBy = currentTimesResourcesParameters.OrderBy,
                        pageNumber = currentTimesResourcesParameters.PageNumber,
                        pageSize = currentTimesResourcesParameters.PageSize
                    });
            }
        }
    }
}
