﻿using System;
using System.Collections.Generic;

namespace Clockwork.API.Entities
{
    public static class ClockworkContextExtensions
    {
        public static void EnsureSeedDataForContext(this ClockworkContext context)
        {
            // first, clear the database.  This ensures we can always start 
            // fresh with each demo.  Not advised for production environments, obviously :-)

            context.CurrentTimes.RemoveRange(context.CurrentTimes);
            context.SaveChanges();

            // init seed data
            var currentTimes = new List<CurrentTime>();

            for (int i = 1; i < 2; i++)
            {
                currentTimes.Add(new CurrentTime
                {
                    ClientIp = (127 + i).ToString() + ".0.0.1",
                    ServerTime = DateTime.Now,
                    UTCTime = DateTime.UtcNow,
                    TimeZoneId = null
                });
            }

            context.CurrentTimes.AddRange(currentTimes);
            context.SaveChanges();
        }
    }
}