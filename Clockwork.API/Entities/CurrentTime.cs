﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Clockwork.API.Entities
{
    public class CurrentTime
    {
        [Key]
        public int CurrentTimeId { get; set; }
        public DateTime ServerTime { get; set; }
        public string ClientIp { get; set; }
        public DateTime UTCTime { get; set; }
        public string TimeZoneId { get; set; }
    }
}
