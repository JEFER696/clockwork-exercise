﻿using Microsoft.EntityFrameworkCore;

namespace Clockwork.API.Entities
{ 
    public class ClockworkContext : DbContext
    {
        public virtual DbSet<CurrentTime> CurrentTimes { get; set; }

        public ClockworkContext(DbContextOptions<ClockworkContext> options)
           : base(options)
        {
        }   
    }
}
