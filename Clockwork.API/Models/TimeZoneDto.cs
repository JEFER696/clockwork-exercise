﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Clockwork.API.Models
{
    public class TimeZoneDto
    {
        public string Id { get; set; }
        public string DisplayName { get; set; }
    }
}
