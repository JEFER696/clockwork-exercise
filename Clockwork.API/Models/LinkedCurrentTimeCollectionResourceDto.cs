﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Threading.Tasks;

namespace Clockwork.API.Models
{
    public class LinkedCurrentTimeCollectionResourceDto
    {
        public IEnumerable<ExpandoObject> Value { get; set; }
        public IEnumerable<LinkDto> Links { get; set; }
    }
}
