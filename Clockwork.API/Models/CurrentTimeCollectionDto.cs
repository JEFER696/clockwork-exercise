﻿using System.Collections.Generic;

namespace Clockwork.API.Models
{
    public class CurrentTimeCollectionDto
    {
        public IEnumerable<CurrentTimeDto> CurrentTimes { get; set; }
        public int TotalCount { get; set; }
    }
}
