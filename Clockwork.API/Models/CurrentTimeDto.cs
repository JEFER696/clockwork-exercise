﻿using System;

namespace Clockwork.API.Models
{
    public class CurrentTimeDto
    {
        public int CurrentTimeId { get; set; }
        public DateTime ServerTime { get; set; }
        public string ClientIp { get; set; }
        public DateTime UTCTime { get; set; }
        public string TimeZoneId { get; set; }
    }
}

