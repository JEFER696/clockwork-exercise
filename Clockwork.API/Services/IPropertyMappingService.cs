﻿namespace Clockwork.API.Services
{
    public interface IPropertyMappingService
    {
        System.Collections.Generic.Dictionary<string, PropertyMappingValue> GetPropertyMapping<TSource, TDestination>();
        bool ValidMappingExistsFor<TSource, TDestination>(string fields);
    }
}