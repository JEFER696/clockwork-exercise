﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Clockwork.API.Entities;
using Clockwork.API.Helpers;

namespace Clockwork.API.Services
{
    public interface IClockworkRepository
    {
        Task AddCurrentTime(CurrentTime currenTime);
        Task<PagedList<CurrentTime>> GetCurrentTimes(CurrentTimesResourcesParameters currentTimesResourcesParameters);
        Task<bool> Save();
    }
}