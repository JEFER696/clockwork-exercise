﻿using Clockwork.API.Entities;
using Clockwork.API.Helpers;
using Clockwork.API.Models;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;

namespace Clockwork.API.Services
{
    public class ClockworkRepository : IClockworkRepository
    {
        private ClockworkContext _context;
        private IPropertyMappingService _propertyMappingService;

        public ClockworkRepository(ClockworkContext context,
            IPropertyMappingService propertyMappingService)
        {
            _context = context;
            _propertyMappingService = propertyMappingService;
        }

        public async Task AddCurrentTime(CurrentTime currenTime)
        {
            await _context.CurrentTimes.AddAsync(currenTime);
        }
                
        public async Task<PagedList<CurrentTime>> GetCurrentTimes(
            CurrentTimesResourcesParameters currentTimesResourcesParameters)
        {
            var collectionBeforePaging = 
                await _context.CurrentTimes.ApplySort(currentTimesResourcesParameters.OrderBy,
                _propertyMappingService.GetPropertyMapping<CurrentTimeDto, CurrentTime>())
                .ToListAsync();
            
            var currentTimes = PagedList<CurrentTime>.Create(collectionBeforePaging.AsQueryable(),
                currentTimesResourcesParameters.PageNumber,
                currentTimesResourcesParameters.PageSize);

            return currentTimes;
        }

        public async Task<bool> Save()
        {
            return (await _context.SaveChangesAsync() >= 0);
        }

    }
}

