﻿using System;

namespace Clockwork.API.Helpers
{
    public static class TimeZoneExtensions
    {
        /// <summary>
        /// Checks if time zone is valid.
        /// </summary>
        /// <param name="timeZoneId"></param>
        /// <returns></returns>
        public static bool IsValid(this string timeZoneId)
        {
            if (!string.IsNullOrWhiteSpace(timeZoneId))
            {
                try
                {
                    return TimeZoneInfo.FindSystemTimeZoneById(timeZoneId) != null ? true : false;
                }
                catch (Exception)
                {
                    return false;
                }
            }
            else
            {
                return true;
            }
        }

        /// <summary>
        /// Get Time Zone Display Name
        /// </summary>
        /// <param name="timeZoneId"></param>
        /// <returns></returns>
        public static string ToDisplayName(this string timeZoneId)
        {
            if (!string.IsNullOrWhiteSpace(timeZoneId))
            {
                try
                {
                    var timeZoneInfo = TimeZoneInfo.FindSystemTimeZoneById(timeZoneId);
                    return timeZoneInfo != null ? timeZoneInfo.DisplayName : string.Empty;
                }
                catch (Exception e)
                {
                    return string.Empty;
                }
            }
            else
            {
                return string.Empty;
            }
        }
    }
}

