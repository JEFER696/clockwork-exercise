﻿using System;

namespace Clockwork.API.Helpers
{
    public static class DateTimeExtensions
    {
        /// <summary>
        /// Converts a time to the time in a particular time zone.
        /// </summary>
        /// <param name="dateTime"></param>
        /// <param name="timeZoneId"></param>
        /// <returns></returns>
        public static DateTime ConvertTime(this DateTime dateTime, string timeZoneId)
        {
            if (!string.IsNullOrWhiteSpace(timeZoneId))
            {
                TimeZoneInfo infoTime = TimeZoneInfo.FindSystemTimeZoneById(timeZoneId);

                if (infoTime != null)
                {
                    //TODO
                    try
                    {
                        return TimeZoneInfo.ConvertTime(dateTime, infoTime);
                    }
                    catch (Exception e)
                    {

                        throw new Exception("DateTime convertion to Time Zone failed.");
                    }
                   
                }
            }

            return dateTime;
        }
    }
}
